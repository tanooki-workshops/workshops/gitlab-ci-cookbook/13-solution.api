# 13-solution.api

registry.gitlab.com/tanooki-workshops/workshops/gitlab-ci-cookbook/12-solution.monorepo/semgrep:latest
registry.gitlab.com/tanooki-workshops/workshops/gitlab-ci-cookbook/12-solution.monorepo/gitleaks:latest

  image: 
    name: registry.gitlab.com/simplify-devops/gitleaks:latest
    entrypoint: [""]

gitleaks --path=${directory} --verbose --report=gitleaks.json

